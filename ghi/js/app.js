function createCard(name, description, pictureUrl, startDate, endDate, locationName) {
    const formattedStartDate = new Date(startDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    const formattedEndDate = new Date(endDate).toLocaleDateString(undefined, { year: 'numeric', month: 'long', day: 'numeric' });
    return `
        <div class="card my-3 shadow-sm">
            <img src="${pictureUrl}" class="card-img-top" alt="Image of ${name}">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
                <p class="card-text">${description}</p>
                <div class="card-footer text-muted">
                    Starts: ${formattedStartDate} - Ends: ${formattedEndDate}
                </div>
            </div>
        </div>
    `;
}
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const cardRow = document.getElementById('card-row');
    let columnIndex = 0;
    const columns = [];
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error(`Failed to fetch conferences. Server responded with ${response.status}: ${response.statusText}`);
        }
        const data = await response.json();
        data.conferences.forEach(async (conference, index) => {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (!detailResponse.ok) {
                throw new Error(`Failed to fetch details for conference ${conference.name}. Server responded with ${detailResponse.status}: ${detailResponse.statusText}`);
            }
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = details.conference.starts;
            const endDate = details.conference.ends;
            const locationName = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate, locationName);
            if (!columns[columnIndex]) {
                columns[columnIndex] = document.createElement('div');
                columns[columnIndex].className = 'col-md-4';
                cardRow.appendChild(columns[columnIndex]);
            }
            columns[columnIndex].innerHTML += html;
            columnIndex = (columnIndex + 1) % 3;
        });
    } catch (e) {
        console.error(e);
        cardRow.innerHTML = `
            <div class="alert alert-danger" role="alert">
                Error: ${e.message}
            </div>
        `;
    }
});
